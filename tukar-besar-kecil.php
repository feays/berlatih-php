<?php
    function tukar_besar_kecil($string){
    //kode di sini
        $huruf=["abcdefghijklmnopqrstuvwxys"];
        $max = strlen($string)-1; $x=0;
        while ($x<=$max){
          $a= strpos($huruf, $string[$x]);
          if ($a==null){
            $string[$x]=strtolower($string[$x]);
          }else{
            $string[$x]=strtoupper($string[$x]);
          }
        }

        return $string."<br>";
    }

        // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
