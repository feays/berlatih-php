<?php
    function ubah_huruf($string)
    {
    //kode di sini
      $huruf=["a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
              "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
              "w", "x", "y", "z"];
      $max = strlen($string)-1; $x=0; $y=0;
      $mx=count($huruf)-1; $y=0;
      while ($x<=$max) {
        $w=substr($string, $x, 1);
        while ($w <> $huruf[$y]) {
          $y+=1;
        }
        if ($y==$mx){
          $string[$x]=$huruf[1];
        }elseif ($y<$mx) {
          $string[$x]=$huruf[$y+1];
        }
        $y=0;
        $x+=1;
      }
      return ($string)."<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

?>
